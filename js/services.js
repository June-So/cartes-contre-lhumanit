//Récupére la carte noire en cours de partie
app.factory('blackCardTurn', ['$http', function($http){
  return {
    get: function(callback){
      $http.get('/controllers/room.ajax.php',{ params:{ seeTurn : ''}}).
      success(function(data, status){
        callback(data,status);
      }).
      error(function(error, status){
        callback(error, status);
      });
    }
  }
}]);

//Récupére les cartes proposé pour le vote
app.factory('seePickCards', ['$http', function($http){
  return {
    get: function(callback){
      $http.get('/controllers/room.ajax.php',{ params:{ seePickCards : ''}}).
      success(function(data, status){
        callback(data,status);
      }).
      error(function(error, status){
        callback(error, status);
      });
    }
  }
}]);

//Récupére la liste des joueurs et leurs status
app.factory('seePlayerList', ['$http', function($http){
  return {
    get: function(callback){
      $http.get('/controllers/room.ajax.php',{ params:{ seePlayerList : ''}}).
      success(function(data, status){
        callback(data,status);
      }).
      error(function(error, status){
        callback(error, status);
      });
    }
  }
}]);

//Récupére la main d'un joueur
app.factory('playerHand', ['$http', function($http){
  return {
    get: function(callback){
      $http.get('/controllers/room.ajax.php',{ params:{ seeHand : ''}}).
      success(function(data, status){
        callback(data,status);
      }).
      error(function(error, status){
        callback(error, status);
      });
    }
  }
}]);

//Récupére le status d'une game
  app.factory('statusGame', ['$http', function($http){
  return {
    get: function(callback){
      $http.get('/controllers/room.ajax.php',{ params:{ seeStatus : ''}}).
      success(function(data, status){
        callback(data,status);
      }).
      error(function(error, status){
        callback(error, status);
      });
    }
  }
}]);

//Récupére les cartes proposé pour le vote
app.factory('seeWinner', ['$http', function($http){
  return {
    get: function(callback){
      $http.get('/controllers/room.ajax.php',{ params:{ seeWinner : ''}}).
      success(function(data, status){
        callback(data,status);
      }).
      error(function(error, status){
        callback(error, status);
      });
    }
  }
}]);
