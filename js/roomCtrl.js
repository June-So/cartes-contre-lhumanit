app.controller('RoomCtrl',['$scope','blackCardTurn','playerHand','$http','seePickCards','statusGame','seePlayerList','$interval','seeWinner',
 function($scope,blackCardTurn,playerHand,$http,seePickCards,statusGame,seePlayerList,$interval,seeWinner){
  // objet carte noire tiré pour le tour
  blackCardTurn.get(function(data){
    $scope.cardB = data;
  });
  $scope.updtblackCardTurn = function(){
    blackCardTurn.get(function(data){
      $scope.cardB = data;
    });
  }
  $interval($scope.updtblackCardTurn, 3000);

  // liste des proposions de carteW
  seePickCards.get(function(data){
    $scope.pickCards = data;
  });

  // liste des cartes des mains d'un joueur
  playerHand.get(function(data){
    $scope.hand = data;
  });

  //statut de la game
  statusGame.get(function(data){
    $scope.statusG = data;
  });
  $scope.updtStatusG = function(){
    statusGame.get(function(data){
      $scope.statusG = data;
    })
  }
  $interval($scope.updtStatusG, 3000);

  $scope.isResult = function(){
    if($scope.statusG == 'resultat'){
      return true;
    } else {
      return false;
    }
  }
  //liste des joueurs
  seePlayerList.get(function(data){
    $scope.playerList = data;
  });
  $scope.updtPlayer = function(){
    seePlayerList.get(function(data){
      $scope.playerList = data;
    })
  }
  $interval($scope.updtPlayer, 3000);


//liste des joueurs
seeWinner.get(function(data){
  $scope.winner = data;
});
$scope.updtWinner = function(){
  seeWinner.get(function(data){
    $scope.winner = data;
  })
}
$interval($scope.updtWinner, 3000);
}]);
