<?php include 'controllers/index.ctrl.php' ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Cartes contre l'humanité</title>
    <link rel="stylesheet" href="css/framework.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>

    <h1 class="center">ALPHA - Cartes contre l'humanité </h1>

    <div class="contain">
      <?php if(!isset($_SESSION['pseudo']) || empty($_SESSION['pseudo'])){?>

        <div class="connexionSession col-offset-3">
          <h2 class="center">Rejoignez le côté obscur avant, qu'il n'y ait plus de cookies :</h2>
          <form class="form-action" action="index.php" method="post">
            <label for="pseudo">Choisissez votre nom de Super Vilain :</label>
            <input type="text" name="pseudo"/>
            <button type="submit">Entrez</button>
          </form>
        </div>

      <?php } else { ?>
        <div class="goGame col-offset-3 col-6">
          <p class="center">Bienvenue parmis nous <?= $_SESSION['pseudo'] ?>, voici votre cookie</p>
          <!-- * Creation d'une game rapide -->
          <form class="center" action="room.php" method="post">
            <input name="create" value="" hidden/>
            <button type="submit">Creer une game rapide</button>
          </form>
      <!-- * Creation d'une room privée ($mdp,$nom,$id[auto]) -->
      <!-- * Creation d'une room public ($nom) -->
      <!-- * Rejoindre une room privée ($mdp,id/nom) -->
      <!-- * Rejoindre une room public ($id/nom) -->
        <form class="center" action="room.php" method="post">
          <button type="submit">Rejoindre une game</button><br/>
          <input type="number" name="join" value=""/>
        </form>
      <!-- * Rejoindre une room aléatoire (si aucune n'a de la place en créer une random) -->
      <?php } ?>
    </div>
  </body>
</html>
