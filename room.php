<?php include 'controllers/room.ctrl.php' ?>
<!-- <!DOCTYPE html> -->
<html>
<head>
  <meta charset="utf-8">
  <title>ROOM - <?= $roomParty->partyId() ?></title>
  <link rel="stylesheet" href="css/framework.css">
  <link rel="stylesheet" href="css/style.css">
  <!-- Script Partagé  -->
  <script type="text/javascript" src="assets/angular/angular.min.js"></script>
  <!-- Script Dépendance -->
  <script src="assets/angular/angular-route.min.js"></script>
  <!-- Script Module -->
  <script type="text/javascript" src="js/index.js"></script>
  <!-- Script Services -->
  <script type="text/javascript" src="js/services.js"></script>
  <!-- Script Controller-->
  <script type="text/javascript" src="js/roomCtrl.js"></script>
  <script type="text/javascript" src="js/spoonyu.js"></script>
</head>

<body class="room">
  <div ng-app="CardsAgainst">
    <div class="roomParty" ng-controller="RoomCtrl">
      <!-- Contenu de la Room -->

      <div class="row">
        <div class="col-10">
          <h1 class="col-12 center">Room n°<?= $roomParty->partyId() ?> - Stade de la game : {{ statusG }} || <?= $roomParty->timeGame() ?></h1>
          <!-- carte noire -->
          <!-- Apparait tout le temps sauf dans la salle d'atente -->
          <div class="cards blackC col-2">
            {{cardB.textCard}}
            {{cardB.pickCard}}
          </div>
          <!-- Résultat de la game -->

          <div ng-show="isResult()" class="result col-12">
            <p>AND THE WINNER IS ... {{winner.player}} avec {{winner.nbrVote}} votes!</p>
            <div class="col-2 cards whiteC">
              {{winner.textCard}}
            </div>
          </div>
          <hr>
          <!-- carte de candidates au vote -->
          <!-- Pendant le tour de vote Quand tout le monde à voté -->
          <?php if($roomParty->timeGame() == 'voteTime'){ ?>
            <div class="propositions col-12">
              <h2>Votez pour votre carte préféré</h2>
              <form class="form-ninja" method="post" action="">
                <div class="col-2" ng-repeat="voteC in pickCards">
                  <input name="sendVote" value="{{voteC.pickCard}}" hidden/>
                  <button type="submit" class="cards whiteC" onclick="sendvote()">{{voteC.textCard}}</button>
                </div>
              </form>
            </div>
          <?php } ?>
          <!-- cartes, main du joueur -->
          <!-- Tout le temps excepté file d'attente -->
          <div class="hand col-12">
            <div class="col-2" ng-repeat="card in hand">
              <?php if($player->status() == 'se décide' || $player->status() == 'envoyé'){ ?>
              <form class="form-ninja" method="post" action="">
                <input type="number" name="sendPickCard" value="{{card.idCardW}}" hidden/>
                <input type="number" name="sendSlotcard" value="{{card.slotcard}}" hidden/>
                <button type="submit" class="cards whiteC" onclick="playCard()">{{card.textCard}}</button>
              </form>
              <?php } ?>
            </div>
          </div>
        </div>
        <!-- liste des joueurs -->
        <!-- Tout le temps; besoin de plus de visuel sur les status -->
        <div class="col-2 playerList">
          <h2 class="center">Liste des joueurs</h2>
          <ul>
            <li ng-repeat="player in playerList">{{player.player}}, {{player.points}} points, {{player.status}}</li>
          </ul>
          <form method="post" action="">
            <input type="text" name="start" hidden/>
          <button type="submit" class="center" onclick="startGame()">START</button>
        </form>
        <p>Si la partie "clignote" ce n'est pas un bug, vous êtes en attente des autres joueurs.</p>
          <p>Phase en attente :<br/> Clicquez sur start pour signalez que vous êtes prêt à jouer.</p>
          <p>Phase start :<br/>  Choisissez une réponse parmis les cartes de votre main.</p>
          <p>Phase voteTime :<br/>  Votez pour votre proposition préférée.</p>
      </div>
      </div>

    </div>
  </div>
  </body>
  </html>
