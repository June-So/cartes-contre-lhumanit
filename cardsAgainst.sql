-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 31, 2016 at 02:11 PM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cardsAgainst`
--

-- --------------------------------------------------------

--
-- Table structure for table `blackCards`
--

CREATE TABLE `blackCards` (
  `id` int(11) NOT NULL,
  `textCard` varchar(255) NOT NULL,
  `pick` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blackCards`
--

INSERT INTO `blackCards` (`id`, `textCard`, `pick`) VALUES
(4, 'A suivre sur Eurosport : Les Championnats du Monde de ______.', 1),
(5, 'Quel est le plus généreux des cadeaux?', 1),
(6, 'Je m\'en serais tiré sans ______.', 1),
(7, 'Qu\'est-ce qui peut être drôle avant de devenir bizarre?', 1),
(8, 'Dans les siècles à venir, les historiens s\'accorderont pour dire que ______ a marqué le début du déclin américain.', 1),
(9, 'Les membres de l\'élite bourgeoise parisienne paient des milliers d\'euros pour essayer ______.', 1),
(10, 'Ce mois-ci dans Gala: "Pimentez votre vie sexuelle en amenant ______ dans le lit".', 1),
(11, 'Jacadi a dit ______.', 1),
(12, 'Que Dieu bénisse la France et ______.', 1),
(13, 'Une étude fascinante a démontré que les chimpanzé ont développé leur propre version primitive de ______.', 1),
(14, 'La semaine prochaine dans C\'est quoi l\'amour: Comment discuter avec votre enfant de ______.', 1),
(15, 'Il n\'existe que deux certitudes absolues: la mort et ______.', 1),
(16, 'Votre persistance est admirable, ô preux chevalier, mais vous ne gagnerez pas mon coeur seulement avec ______', 1),
(17, 'Ma réhabilitation a commencé quand j\'ai rejoint un groupe de soutien pour les victimes de ______.', 1),
(18, 'Pourquoi j\'ai le moral à 0 ?', 1),
(19, 'J\'ai abandonné le Times Up pour toujours quand ma mère a dû mimer ______.', 1),
(20, 'Durant sa crise de la quarantaine, mon père s\'est vachement intéressé à ______.', 1),
(21, '2h du mat\' dans la ville qui ne dort jamais. La porte s\'ouvre et elle entre. Quelque chose dans son regard me dit qu\'elle cherche ______.', 1),
(22, 'Qu\'est-ce qui a compliqué les choses au camp nudiste?', 1),
(23, 'Je suis à peu près certain d\'être défoncé là tout de suite, parce que je suis complètement fasciné par ______.', 1),
(24, 'Pourquoi l\'orgie s\'est-elle brusquemement arrêtée ?', 1),
(25, 'Il rôde dans la nuit, il a besoin de chair fraîche. Cet été, personne n\'est à l\'abri de ______.', 1),
(26, 'Avant de me présenter aux élections présidentielles, je dois effacer toute preuve de mon implication dans ______.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE `player` (
  `points` mediumint(9) DEFAULT '0',
  `player` varchar(42) DEFAULT NULL,
  `partyId` int(11) NOT NULL,
  `status` varchar(42) DEFAULT NULL,
  `timestamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `playerHands`
--

CREATE TABLE `playerHands` (
  `partyId` int(11) NOT NULL,
  `slotcard` tinyint(4) NOT NULL,
  `idCardW` int(11) DEFAULT NULL,
  `player` varchar(42) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roomParty`
--

CREATE TABLE `roomParty` (
  `id` int(11) NOT NULL,
  `blackCardTurn` int(11) DEFAULT NULL,
  `timeGame` varchar(42) DEFAULT NULL,
  `numTurn` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `turn`
--

CREATE TABLE `turn` (
  `blackCard` int(11) DEFAULT NULL,
  `player` varchar(42) DEFAULT NULL,
  `pickCard` int(11) DEFAULT NULL,
  `nbrVote` tinyint(4) NOT NULL DEFAULT '0',
  `partyId` int(11) NOT NULL,
  `numTurn` tinyint(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `whiteCards`
--

CREATE TABLE `whiteCards` (
  `id` int(11) NOT NULL,
  `textCard` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `whiteCards`
--

INSERT INTO `whiteCards` (`id`, `textCard`) VALUES
(32, '2 Girls 1 Cup.'),
(29, 'Batifoler.'),
(14, 'BATMAN!!!'),
(55, 'Beaucoup de bruit pour rien.'),
(39, 'Boire tout seul.'),
(40, 'Des abdominaux spectaculaires.'),
(36, 'Des bébés chiens !'),
(28, 'Du sexe surprise !'),
(42, 'Du sperme de baleine.'),
(52, 'Etre un sale con envers les enfants.'),
(49, 'Faire la moue.'),
(41, 'Faire le bon choix.'),
(12, 'François Ba des biscuits apéro.'),
(50, 'Gargamel.'),
(16, 'L\'abstinence.'),
(25, 'L\'Amerique.'),
(19, 'L\'auto-cannibalisme.'),
(24, 'L\'inceste.'),
(45, 'La capacité d\'écoute.'),
(35, 'La destruction mutuelle assurée.'),
(22, 'La fête du slip.'),
(6, 'La Macarena.'),
(5, 'La Sainte Bible.'),
(20, 'La série des "Fais-moi peut !"'),
(33, 'La tension sexuelle.'),
(54, 'Laisser un message maladroit sur un répondeur.'),
(17, 'Le bibendum Michelin.'),
(38, 'Le Ku Kux Klan.'),
(31, 'Le lancer de nain sur une cible velcro.'),
(51, 'Le Stade de France.'),
(47, 'Les cols portés relevés.'),
(53, 'Les enfants tenus en laisse.'),
(27, 'Les équipes chinoises de gymnastiques.'),
(34, 'Les gens qui sentent leurs chaussettes.'),
(43, 'Les inondations.'),
(9, 'Les juifs avec une coupe afro.'),
(44, 'Les Oompas Loompas'),
(10, 'Les P\'tits Filous Tubes.'),
(15, 'Les vikings.'),
(23, 'Mes parties génitales.'),
(13, 'Michaël Jackson'),
(26, 'Mourir de la dysenterie.'),
(21, 'Pour toujours.'),
(7, 'Se chier dessus.'),
(46, 'Se mettre tellemen en colère que ça en donne une érecion'),
(18, 'Son Altesse royale, la reine Elizabeth II.'),
(8, 'Un anus maquillé.'),
(11, 'Une branlette tristement exécutée.'),
(37, 'Une fête d\'anniversaire ratée.'),
(56, 'Une mycose.'),
(30, 'Une tortue vicieuse'),
(48, 'World of Warcraft.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blackCards`
--
ALTER TABLE `blackCards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `textCard` (`textCard`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD UNIQUE KEY `player` (`player`,`partyId`);

--
-- Indexes for table `playerHands`
--
ALTER TABLE `playerHands`
  ADD UNIQUE KEY `partyId` (`partyId`,`slotcard`,`player`);

--
-- Indexes for table `roomParty`
--
ALTER TABLE `roomParty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turn`
--
ALTER TABLE `turn`
  ADD UNIQUE KEY `player` (`player`,`partyId`);

--
-- Indexes for table `whiteCards`
--
ALTER TABLE `whiteCards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `textCard` (`textCard`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blackCards`
--
ALTER TABLE `blackCards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `roomParty`
--
ALTER TABLE `roomParty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=476;
--
-- AUTO_INCREMENT for table `whiteCards`
--
ALTER TABLE `whiteCards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
