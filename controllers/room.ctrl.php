<?php session_start();
include $_SERVER['DOCUMENT_ROOT'].'models/player.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/blackCard.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/whiteCard.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/db.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/room.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/turnManagement.class.php';

$datebase = new db();
$DB = $datebase->connectDb();

//Si l'utilisateur est arrivé en créant une partie
if(isset($_POST['create']) && isset($_SESSION['pseudo']) && !empty($_SESSION['pseudo']) && $_SESSION['game'] == null){

  //Creation d'une room et stock l'id dans la session
  $roomParty = new room($DB);
  $roomParty->createGame();
  $_SESSION['roomPlay'] = $roomParty->partyId();
  $roomId = $roomParty->partyId();
  //L'utilisateur est ajouté à liste des joueurs
  $player = new player($DB,$roomId,$_SESSION['pseudo']);
  $player->addPlayer();

  //Si l'utilisateur est arrivé en rejoignant une game pendant la phase d'attente
}else if(isset($_POST['join']) && $_SESSION['game'] == null){
  $joinRoomId = htmlspecialchars($_POST['join']);
  if(empty($joinRoomId)){
    $roomParty = new room($DB);
    $roomParty->getRoomRandom();
  } else {
    $roomParty = new room($DB,$joinRoomId);
  }
  //charge la partie
  $_SESSION['roomPlay'] = $roomParty->partyId();
  $roomParty->loadGame();
  $roomId = $roomParty->partyId();
  //Si la partie n'a pas commencée ajoute a la liste des joueurs
  if($roomParty->timeGame == 'en attente'){
    $player = new player($DB,$roomId,$_SESSION['pseudo']);
    $player->addPlayer();
    //Si la partie à déjà commencée || et que le pseudo ne fait pas partie de la liste des joueurs
  } else {
    echo 'erreur : la partie a deja commencé ou nous ne pouvons comptez votre histoire par manque de pseudonyme, vous allez être redirigé vers l\'accueil';
    sleep(3);
    header('Location:index.php');
  }

} else {
  //Rechargement de la page
  $roomParty = new room($DB,$_SESSION['roomPlay']);
  //Charge le statut de la game,
  //Charge la carte noire
  $roomParty->loadGame();
  $roomId = $roomParty->partyId();
  $player = new player($DB,$roomId,$_SESSION['pseudo']);
  $player->getStatus();
  $player->loadHand();
}
$_SESSION['game'] = 'exist';

$player->sendTimestamp();
$turn = new turnManagement($DB,$roomId);

/** Phase d'attente, deconnexion si absent, le joueur peut passer à "ready", le dernier prêt créé les mains et tire une carte noire**/
if($roomParty->timeGame() == 'en attente'){
  //Envoi un timestamp et verifie les joueurs actifs
  $roomParty->playerLeave();

  if(isset($_POST['start'])){
    $player->changeStatus('ready');
    //Compte le nombre de joueur 'ready'
    //Si tout le monde est prêt passe la game en ready
    $playerCount = $turn->countPlayers();
    $playerReady = $turn->countPlayerStatus('ready');
    //Si tout les joueurs actif sont ready
    if($playerReady == $playerCount){
      $players = $roomParty->playerList();
      $roomParty->createHands($players);
      //Tire une carte noire serveur(roomParty et tour) et objet
      //S generation du carte noire aléatoire
      $blackCard = new blackCard($DB);
      //Stocke la carte dans la room
      $roomParty->blackCard = $blackCard;
      $roomParty->sendBlackCard($blackCard->id());
      $roomParty->changeStatus('start');
    } else {
      header('refresh:3;url=room.php');
    }
  } else if($player->status() == 'ready'){
    header('refresh:3;url=room.php');
  }
}

if($roomParty->timeGame() == 'start'){
  echo 'you are supposed to be here';
  if($player->status() == 'ready'){
    //Complete une main pour le joueur SI ELLE EST VIDE
    $player->pickACard();
    $player->changeStatus('se décide');
  }

  //Choisis et envoi une carte de la main du joueur
  //Retire la carte de la main du joueur
  //passe le status du joueur et de la game en picktime(wait)
  if($player->status() == 'se décide' && isset($_POST['sendPickCard'])){
      //S envoi du joueur et de l'id carte pick
      $pickCard = $_POST['sendPickCard'];
      $slotcard = $_POST['sendSlotcard'];
      $turn->sendPickCard($player,$pickCard);
      //S retire la carte de la base de donnée
      $player->playACard($slotcard);
//----Si le (nombre de pick) == (nombre de carte envoyé alors :)
      $player->changeStatus('envoyé');
      //Verifie si tout les joueurs ont envoyé leurs carte pour passer en phase de vote
      $nbrSendCard = $turn->countPickCard();
      $playerCount = $turn->countPlayers();
      if($nbrSendCard == $playerCount){
        $roomParty->changeStatus('voteTime');
      } else {
        header('refresh:3;url=room.php');
      }
    } else if($player->status() == 'envoyé'){
      header('refresh:3;url=room.php');
    }
  }

if($roomParty->timeGame() == 'voteTime'){
  if(isset($_POST['sendVote']) && !empty($_POST['sendVote']) && $player->status() == 'envoyé'){
    //Verifie que ce n'est pas la carte du joueur
      //renvoi le joueur à qui appartiens la carte envoyé
      $cardPlayer = $turn->isPlayerCard($_POST['sendVote']);
      if( $cardPlayer['player'] == $player->pseudo() ){
        echo 'Vous ne pouvez pas vous aimer autant !';
      } else {
      //Enregistre le vote +1 dans le tour(serveur)

      $turn->sendVote($_POST['sendVote']);

      $player->changeStatus('a voté');
      //Verifie si tout le monde à voté
      $nbrVote = $turn->countVote();
      $playerCount = $turn->countPlayers();
      //Passe à la phase résultat
      if($nbrVote == $playerCount){
        echo 'WHOS THE BIG WINNER ?';
        $roomParty->changeStatus('resultat');
      } else {
        header('refresh:2;url=room.php');
      }
    }
  } else if($player->status() == 'a voté'){
      header('refresh:2;url=room.php');
  }
}
//Quand on bloque sur quelque chose,
//dans le code ou dans la vie,
//il ne faut jamais choisir de ne rien faire,
//toujours essayer quelque chose
//meme si ça rate et surtout si ça rate
//C'est la meilleure façon d'apprendre et la seule façon d'avancer

if($roomParty->timeGame() == 'resultat'){
  $winner = $turn->winner();
  if($player->pseudo() == $winner['player']){
    $player->winPoint(2);
  }
  //nettoyer le tour
  $player->changeStatus('ready');
  $playerCount = $turn->countPlayers();
  $playerReady = $turn->countPlayerStatus('ready');
  //Si tout les joueurs actif sont ready
  if($playerReady == $playerCount){
    //nouvelle carte noire
    $blackCard = new blackCard($DB);
    //Stocke la carte dans la room
    $roomParty->blackCard = $blackCard;
    $roomParty->sendBlackCard($blackCard->id());
    sleep(10);
    $turn->cleanTurn();
    $roomParty->changeStatus('start');
  }
  $player->changeStatus('ready');
  header('refresh:3;url=room.php');
}


/* - Affichage de la carte gagnante
   - Ajout des points au joueurs gagnant
   - Redémarrer le tour si ce n'est pas le dernier sinon donner le gagnant final
   - Empecher les envois de trop
   - Securiser
*/


?>
