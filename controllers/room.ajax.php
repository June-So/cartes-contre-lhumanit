<?php
session_start();
include $_SERVER['DOCUMENT_ROOT'].'models/player.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/blackCard.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/whiteCard.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/db.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/room.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/turnManagement.class.php';


$datebase = new db();
$DB = $datebase->connectDb();
$roomParty = new room($DB,$_SESSION['roomPlay']);
$roomParty->loadGame();
$turn = new turnManagement($DB,$roomParty->partyId());
$player = new player($DB,$roomParty->partyId(),[ 'pseudo' => $_SESSION['pseudo']]);


//Envoi les cartes de la main du joueur
if(isset($_GET['seeHand'])){
  echo json_encode($turn->getHand($_SESSION['pseudo']));
}

//Retourn les details, cardB
if(isset($_GET['seePickCards'])){
  echo json_encode($turn->getPickCards($_SESSION['pseudo']));
}

//Retourn les details, cardB
if(isset($_GET['seeTurn'])){
  echo json_encode($turn->getBlackCard());
}

if(isset($_GET['seePlayerList'])){
  echo json_encode($roomParty->playerList());
}

if(isset($_GET['seeStatus'])){
  echo $roomParty->getStatusGame();
}

if(isset($_GET['seeWinner'])){
  echo json_encode($turn->winner());
}




 ?>
