<?php
class turnManagement{
  /**
   *@var int $blackCard, id aléaotoire de la table blackCards
   *@var string $player, pseudo de la session de l'utilisateur
   *@var int $pickCard, id d'une carte de la main du joueur
   *@var int $nbrVote, nombre de vote recu par la carte
   *@var int $numTurn, numero du tour en cours
   *@var int $partyId, Id de la partie en cours
   *@var PDO $db, connexion à la base de donnée
   *
   *@method sendBlackCard(), envoie LA carte noire dans la room
   *@method getBlackCard(), récupére les détails la carte noire du tour
   *@method sendPickCard(), envoie la carte choisit par le jour dans tour
   *@method getHand(), return la liste des cartes de la main d'un joueur
   *@method getPickCards(), renvoi la liste des cartes envoyé
   *
   *@method Compte le nombre de joueurs ayant une main pleine
   *@method Compte le nombre de joueurs qui ont envoyé une carte dans le tour
   *@method Compte le nombre de joueurs
   *@method Compte le nombre de joeurs ayant un staut '$statut'
   *@method Compte la somme du nombre de vote envoyé
   *@method Envoi +1 dans pour la carte selectioné
   *@method Retourne le joueur ayant le plus de vote
   */
  private $blackCard = null;
  private $player;
  private $pickCard;
  private $nbrVote;
  private $numTurn = 1;
  private $partyId;
  private $db;

  /**
   *@param int id de la partie en cours
   *@param PDO connexion à la base de donnée
   */
  public function __construct($db,$partyId){
    $this->partyId = $partyId;
    $this->db = $db;
  }

  /**
   *@return renvois toute les infos de la carte noire en cours
   */
  public function getBlackCard(){
    $req = $this->db->prepare('SELECT blackCardTurn,textCard,pick FROM roomParty INNER JOIN blackCards ON roomParty.blackCardTurn = blackCards.id WHERE roomParty.id = :partyId');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->execute();
    return $req->fetch(PDO::FETCH_ASSOC);
  }

  /**
  *@param str $player, pseudo du joueur
  *@param int $pickCard id de la carte choisit par le J
  *@return succes de l'ajout du pick du joueur dans la bdd
  */
  public function sendPickCard($player,$pickCard){
    $req = $this->db->prepare('INSERT INTO turn (player,blackCard,pickCard,partyId,numTurn) VALUES (:pseudo,:blackCard,:pickCard,:partyId,:numTurn)');
    $req->bindValue('pseudo',$player->pseudo(), PDO::PARAM_STR);
    $req->bindValue('blackCard',$this->blackCard, PDO::PARAM_INT);
    $req->bindValue('pickCard',$pickCard, PDO::PARAM_INT);
    $req->bindValue('partyId',$this->partyId, PDO::PARAM_INT);
    $req->bindValue('numTurn',$this->numTurn, PDO::PARAM_INT);
    return $req->execute();
  }

  /**
  *@return array_assoc, contenu de chaque slot du joueur pour la partie
  */
  public function getHand($pseudo){
    $req = $this->db->prepare('SELECT slotcard,idCardW,textCard FROM playerHands INNER JOIN whiteCards ON playerHands.idCardW = whiteCards.id WHERE partyId = :partyId AND player = :player');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->bindValue('player',$pseudo,PDO::PARAM_STR);
    $req->execute();
    return $req->fetchAll(PDO::FETCH_ASSOC);
  }

  public function getPickCards($pseudo){
    $req = $this->db->prepare('SELECT pickCard,textCard FROM turn INNER JOIN whiteCards ON turn.pickCard = whiteCards.id WHERE partyId = :partyId AND player <> :player');
    $req->bindValue('player',$pseudo,PDO::PARAM_STR);
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->execute();
    return $req->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
   *@return int nombre de joueurs ayant recu toutes ses cartes
   */
  public function countDistribPlayer(){
    $req = $this->db->prepare('SELECT COUNT(DISTINCT player) AS countDistribCards FROM `playerHands` WHERE partyId = :partyId AND idCardW IS NOT NULL');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->execute();
    $rep = $req->fetch(PDO::FETCH_ASSOC);
    return $rep['countDistribCards'];
  }

  /**
   *@return int nombre de joueurs qui ont envoyé une carte
   */
  public function countPickCard(){
    $req = $this->db->prepare('SELECT COUNT(pickCard) FROM turn WHERE pickCard IS NOT NULL AND partyId = :partyId');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->execute();
    $rep = $req->fetch(PDO::FETCH_ASSOC);
    return $rep['COUNT(pickCard)'];
  }

  /**
   *@return int nombre de joueur
   */
  public function countPlayers(){
    $req = $this->db->prepare('SELECT COUNT(player) FROM player WHERE partyId = :partyId');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->execute();
    $rep = $req->fetch(PDO::FETCH_ASSOC);
    return $rep['COUNT(player)'];
  }

  /**
   *@param str statut du joueur
   *@return int nombre de joueur ayant le statut $statut
   */
  public function countPlayerStatus($status){
    $req = $this->db->prepare('SELECT COUNT(player) FROM player WHERE status = :status AND partyId = :partyId');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->bindValue('status',$status,PDO::PARAM_STR);
    $req->execute();
    $rep = $req->fetch(PDO::FETCH_ASSOC);
    return $rep['COUNT(player)'];
  }

  public function countVote(){
    $req = $this->db->prepare('SELECT SUM(nbrVote) FROM turn WHERE partyId = :partyId');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->execute();
    $rep = $req->fetch(PDO::FETCH_ASSOC);
    return $rep['SUM(nbrVote)'];
  }
  /**
   *@param int $vote id carte choisis par le joueur
   *@return succes de l'ajout du vote
   */
  public function sendVote($vote){
    $req = $this->db->prepare('UPDATE turn SET nbrVote= nbrVote+1 WHERE pickCard = :idCard AND partyId = :partyId');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->bindValue('idCard',$vote,PDO::PARAM_INT);
    return $req->execute();
  }

  public function winner(){
    $req = $this->db->prepare('SELECT player,nbrVote,textCard FROM turn INNER JOIN whiteCards ON turn.pickCard = whiteCards.id WHERE partyId = :partyId ORDER BY nbrVote DESC LIMIT 1');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->execute();
    return $req->fetch();
  }

  public function cleanTurn(){
    $req = $this->db->prepare('DELETE FROM turn WHERE partyId = :partyId');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    return $req->execute();
  }

  public function isPlayerCard($idCard){
    $req = $this->db->prepare('SELECT player FROM turn WHERE pickCard = :pickCard AND partyId = :partyId ');
    $req->bindValue('pickCard',$idCard);
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->execute();
    return $req->fetch(PDO::FETCH_ASSOC);
  }

  public function blackCard(){
    return $this->blackCard;
  }
  public function setBlackCard($blackCard){
    $this->blackCard = $blackCard;
  }


}
 ?>
