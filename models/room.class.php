<?php
class room{
    /**
     *@var int nombre de joueurs max
     *@var int nombre de tour
     *@var int id de la partie
     *@var str timeGame stade de la partie actuelle
     *@var PDO connexion à la base de donnée
     *@var obj $blackCard carte noire en cours
     *@var int $numTurn numéro du tour actuel
     *@method Creation d'une nouvelle room 'en attente', récupération id
     *@method Charge une game, récupére le status de la game et la carte noire
     *@method Ajoute des joueurs à la room (boucle method player)
     *@method Change le status de la partie, serveur + objet
     *@method Creer une main vide pour tout les joueurs
     *@method Distribue les cartes, remplit les slot null de la main des joueurs (boucle)
     *@method Liste des joueurs présent dans la room
     *@method Supprime un joueurs qui n'est plus sur la page depuis 30s
     *@method Return le status de la game + maj objet
     *@method Envoi de la carte noire dans la room (serveur)
     */
    private $numberPlayer;
    private $numberTurn = 10;
    private $partyId;
    public $timeGame;
    public $blackCard = null;
    private $numTurn;
    private $db;

    /**
     *@param obj PDO $db connexion base de donnée
     *@param int $partyId id de la room
     *@param int $numberPlayer nombre de joueur max
     *@param int $numberTurn nombre de tour max
     *
     */
    public function __construct($db,$partyId = null,$numberPlayer = null, $numberTurn = null){
      $this->db = $db;
      //Create a game
      if($numberPlayer != null){
        $this->setNumberPlayer($numberPlayer);
        $this->setNumberTurn($numberTurn);
      }
      if($partyId != null){
        $this->partyId = $partyId;
      }
    }

      /**
       * Créer une nouvelle room dans la base de donnée et la passé en attente (Serveur+Attribut)
       */
      public function createGame(){
        $this->db->query('INSERT INTO roomParty (timeGame) VALUES (\'en attente\')');
        $this->partyId = (int) $this->db->lastInsertId();
        $this->timeGame = 'en attente';
      }

  /**
  *s Récupére les informations du tour actuel et les places dans l'objet
  */
  public function loadGame(){
    $req = $this->db->prepare('SELECT timeGame,blackCardTurn,numTurn FROM roomParty WHERE id=:id');
    $req->bindValue('id',$this->partyId,PDO::PARAM_INT);
    $req->execute();
    $status = $req->fetch(PDO::FETCH_ASSOC);
    $this->timeGame = $status['timeGame'];
    $this->blackCard = $status['blackCardTurn'];
    $this->numTurn = $status['numTurn'];
  }

    /**
     *ajoute des joueurs à la liste roomParty
     */
    // public function addPlayers($players){
    //   foreach ($players as $player) {
    //     $player->addPlayer();
    //   }
    // }

    /**
     * Change le statut dans la partie dans la base de donnée + Attribut
     */
    public function changeStatus($status){
      $req = $this->db->prepare('UPDATE roomParty SET timeGame=\''.$status.'\' WHERE id = :id');
      $req->bindValue('id',$this->partyId,PDO::PARAM_INT);
      $req->execute();
      $this->timeGame = $status;
    }

    /**
    *Creer une main vide pour chaque joueur dans la base de donnée
     *@param array liste des objets player présent
     */
    public function createHands($players){
      foreach ($players as $player) {
        //@return un tableau avec le contenu de chaque slot de carte
          for($i = 1; $i <= 5; $i++){
            $req = $this->db->prepare('INSERT INTO playerHands (partyId,player,slotcard) VALUES (:partyId,:player,:slotcard)');
            $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
            $req->bindValue('player',$player['player'],PDO::PARAM_STR);
            $req->bindValue('slotcard',$i,PDO::PARAM_INT);
            $req->execute();
          }
        }
      }

    //Passe au tour suivant
    public function nextTurn(){
      $req = $this->db->prepare('UPDATE roomParty SET numTurn = numTurn +1 WHERE id = :id ');
      $req->bindValue('id',$this->partyId,PDO::PARAM_INT);
      $req->execute();
    }

    /**
    * Complete la main de chaque joueur par une carte
     *@param array liste des joueurs
     */
    public function distribCard($players){
      foreach ($players as $player){
        $req = $this->db->prepare('UPDATE playerHands SET idCardW = :idCardW WHERE partyId = :partyId AND player = :player AND slotcard IS NULL');
        $req->bindValue('idCardW',rand(1,4),PDO::PARAM_INT);
        $req->bindValue('player',$player['player'],PDO::PARAM_STR);
        $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
        $req->execute();
      }
    }

    /**
     *@return array_assoc liste de joueurs présent dans la room, leur points et leur statut
     */
    public function playerList(){
      $req = $this->db->prepare('SELECT player,points,status,`timestamp` FROM player WHERE partyId = :partyId');
      $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
      $req->execute();
      return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Supprime un joueur si il n'est plus sur la page
     */
    public function playerLeave(){
      $timestamp_30 = time() - 30;
      $this->db->exec('DELETE FROM player WHERE `timestamp`<'.$timestamp_30.'');
    }

    /**
     *@return str statut de la game
     */
    public function getStatusGame(){
      $req = $this->db->prepare('SELECT timeGame FROM roomParty WHERE id=:id');
      $req->bindValue('id',$this->partyId,PDO::PARAM_INT);
      $req->execute();
      $status = $req->fetch(PDO::FETCH_ASSOC);
      return $this->timeGame = $status['timeGame'];
    }

    /**
      *Permet de faire une jointure
     *@return succes de l'envoi de la carte noire du tour dans la table roomParty
     */

    public function sendBlackCard($cardBId){
      $req = $this->db->prepare('UPDATE roomParty SET blackCardTurn=:card WHERE id=:id');
      $req->bindValue('card',$cardBId,PDO::PARAM_INT);
      $req->bindValue('id',$this->partyId,PDO::PARAM_INT);
      return $req->execute();
    }

    public function bigWinner(){
      $req = $this->db->prepare('SELECT player FROM player WHERE partyId = :partyId ORDER BY points DESC LIMIT 1');
      $req->bindValue('id',$this->partyId,PDO::PARAM_INT);
      $req->execute();
      return $req->fetch(PDO::FETCH_ASSOC);
    }

    public function getRoomRandom(){
      $req = $this->db->query('SELECT id FROM roomParty WHERE timeGame = \'en attente\' ORDER BY RAND() LIMIT 1');
      $id = $req->fetch(PDO::FETCH_ASSOC);
      $this->partyId = $id['id'];
    }

    public function numTurn(){
      return $this->numTurn;
    }
    public function numberTurn(){
      return $this->numberTurn;
    }
    public function partyId(){
      return $this->partyId;
    }

    public function blackCard(){
      return $this->blackCard;
    }

    public function timeGame(){
      return $this->timeGame;
    }

  }
?>
