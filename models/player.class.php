<?php
class player{
  /**
  *@var PDO $db connexion à une base de donnée
  *@var int $partyId id de la partie en cours
  *@var string $pseudo du joueur
  *@var int nombre de point
  *@var status du joueur;
  *@var array $slotcardX, carte X de la main du joueur
  *@method addPlayer(), ajoute un joueur à la liste des joueurs dans la room (serveur)
  *@method loadPlayer(), charge les points le status et la main du joueur
  *@method changeStatus(), change le statut du joueur en ready
  *@method getStatus(), stock et return le statut du joueur
  *@method createHand(), creer une main vide pour le joueur
  *@method loadHand(), charge la main du joueur et les stock dans les attributs slotcardX
  *@method hands(), return main du joueur, tableau contenant les objets $slotcards
  *@method pickACard() remplis les slotcard vide objet & serveur
  *@method playACard(), update la carte du slot joué en null objet & serveur
  *@method winPoint(), ajoute des points au joueurs objet & serveur
  */
  private $db;
  private $partyId;
  private $pseudo;
  private $points = 0;
  private $status;

  public $slotcard1;
  public $slotcard2;
  public $slotcard3;
  public $slotcard4;
  public $slotcard5;

  /**
  *@param PDO connexion à la base de donnée
  *@param int $partyId id de la room en cours
  *@param array_assoc , envoi un pseudo dans les attributs
  */
  public function __construct($db,$partyId,$pseudo){
    $this->setPseudo($pseudo);
    $this->partyId = $partyId;
    $this->db = $db;
  }

  /**
  * Ajoute un joueur et passe son attribut status en attente
  */
  public function addPlayer(){
    $req = $this->db->prepare('INSERT INTO player (partyId,player,`timestamp`,status) VALUES (:partyId,:pseudo,:times,\'en attente\')');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->bindValue('pseudo',$this->pseudo,PDO::PARAM_STR);
    $req->bindValue('times',time(),PDO::PARAM_INT);
    $req->execute();
    $this->status = 'en attente';
  }

  /**
   * Charge les éléments d'un joueur et sa main
   */
  public function loadPlayer(){
    $req = $this->db->prepare('SELECT points,status FROM player WHERE partyId=:partyId AND player=:pseudo');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->bindValue('pseudo',$this->pseudo,PDO::PARAM_STR);
    $rep = $req->execute();
    $this->loadHand();
  }

  /**
   *Change le statut d'un joueur
   *@return le succes de l'ajout du status ready au joueur
   */
  public function changeStatus($status){
    $this->status = $status;
    $req = $this->db->prepare('UPDATE player SET status=:status WHERE partyId = :partyId AND player = :pseudo');
    $req->bindValue('status',$status,PDO::PARAM_STR);
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->bindValue('pseudo',$this->pseudo,PDO::PARAM_STR);
    return $req->execute();
  }

  /**
   *@return le statut du joueur
   */
  public function getStatus(){
    $req = $this->db->prepare('SELECT status FROM player WHERE partyId = :partyId AND player = :pseudo');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->bindValue('pseudo',$this->pseudo,PDO::PARAM_STR);
    $req->execute();
    $rep = $req->fetch(PDO::FETCH_ASSOC);
    $this->status = $rep['status'];
    return $rep['status'];
  }

/**
 * Créer une main vide pour le joueur
 */
  public function createHand(){
    for($i = 1; $i <= 5; $i++){
      $req = $this->db->prepare('INSERT INTO playerHands (partyId,player,slotcard) VALUES (:partyId,:player,:slotcard)');
      $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
      $req->bindValue('player',$this->pseudo,PDO::PARAM_STR);
      $req->bindValue('slotcard',$i,PDO::PARAM_INT);
      $req->execute();
    }
  }

  /**
   *Charge la main d'un joueur et remplis les attributs slotcardX
   */
  public function loadHand(){
    for($i = 1; $i <= 5;$i++){
      $slot = 'slotcard'.$i;
      if($this->$slot == null){
        $this->$slot = new whiteCard($this->db);
        //Ajouter au serveur
        $req = $this->db->prepare('SELECT * FROM playerHands WHERE partyId = :partyId AND player = :player AND slotcard = :slotcard');
        $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
        $req->bindValue('player',$this->pseudo,PDO::PARAM_STR);
        $req->bindValue('slotcard',$i,PDO::PARAM_INT);
        $req->execute();
        $loadcard = $req->fetch(PDO::FETCH_ASSOC);
        $this->$slot = $loadcard['idCardW'];
      }
    }
  }

  /**
   *@return array liste de tout lles attributs slotcardX
   */
  public function hands(){
    $hand = [1 => $this->slotcard1,2 => $this->slotcard2,3 => $this->slotcard3,4 => $this->slotcard4,5 => $this->slotcard5 ];
    return $hand;
  }

  /**
  * Remplit chaque $slotcardX si il est null, ajoute les carte parallélement au serveur dans la table playerHands
  */
  public function pickACard(){
    for($i = 1; $i <= 5;$i++){
      $slot = 'slotcard'.$i;
      if($this->$slot == null){
        $this->$slot = new whiteCard($this->db);
        //Ajouter au serveur
        $req = $this->db->prepare('UPDATE playerHands SET idCardW = :idCardW WHERE partyId = :partyId AND player = :player AND slotcard = :slotcard');
        $req->bindValue('idCardW',$this->$slot->id(),PDO::PARAM_INT);
        $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
        $req->bindValue('player',$this->pseudo,PDO::PARAM_STR);
        $req->bindValue('slotcard',$i,PDO::PARAM_INT);
        $req->execute();
      }
    }
  }

  /**
  * retire une carte de la main du joueur
  *@param int $slot, slotcard envoyé
  *@method Passe en null la valeur de la carte lié au slotcard
  */
  public function playACard($slot){
    //Ajouter au serveur
    $req = $this->db->prepare('UPDATE playerHands SET idCardW = null WHERE partyId = :partyId AND player = :player AND slotcard = :slot');
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->bindValue('player',$this->pseudo,PDO::PARAM_STR);
    $req->bindValue('slot',$slot,PDO::PARAM_INT);
    $req->execute();
  }

  /**
  * Ajoute des points au joueur
  *@param int $pts nombre de point remporter
  *@return succes de l'ajout de point au joueur pour la partie
  */
  public function winPoint($pts){
    $this->points += $pts;
    $req = $this->db->prepare('UPDATE player SET points = points + :points WHERE partyId = :partyId AND player = :player');
    $req->bindValue('points',$pts,PDO::PARAM_INT);
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->bindValue('player',$this->pseudo,PDO::PARAM_STR);
    return $req->execute();
  }

  /**
   * Envoi le timestamp pour verifier la présence du joueur
   */
  public function sendTimestamp(){
    $req = $this->db->prepare('UPDATE player SET `timestamp` = :times WHERE partyId = :partyId AND player = :player' );
    $req->bindValue('player',$this->pseudo,PDO::PARAM_STR);
    $req->bindValue('partyId',$this->partyId,PDO::PARAM_INT);
    $req->bindValue('times',time(),PDO::PARAM_INT);
    $req->execute();
  }

  public function setPseudo($player){
    $this->pseudo = $player;
  }

  public function slotcards1(){
    return $this->slotcard1;
  }
  public function pseudo(){
    return $this->pseudo;
  }

  public function status(){
    return $this->status;
  }
}
?>
