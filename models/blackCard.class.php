<?php
class blackCard{

  /**
  *@var int $id id de la carte
  *@var string $textCard, texte de la carte
  *@var int $pick nombre de carte blanche qui doivent être choisies par les joueurs
  *@var pdo $db, database
  *@method generateBlackCard(), selectionne une carte aléatoire noire
  */

  private $id;
  private $textCard;
  private $pick;
  private $db;

  /**
    *@param PDO $db, connexion à la base de données
    */
  public function __construct($db){
    $this->db = $db;
    $this->generateBlackCard();
  }

  /**
   * Selectionne une carte noire aléatoire et hydrate les valeurs vers les attributs
   *@param int $id id aléatoire d'une carte noire
   *@return array_assoc d'une carte noire aléatoire
   */
  public function generateBlackCard(){
    $req = $this->db->query('SELECT * FROM blackCards ORDER BY RAND() LIMIT 1');
    $card = $req->fetch();
    $this->hydrate($card);
  }

  public function id(){
    return $this->id;
  }
  public function textCard(){
    return $this->textCard;
  }

  public function setId($id){
    $this->id = (int) $id;
  }
  public function setTextCard($textCard){
    $this->textCard = $textCard;
  }
  public function setPick($pick){
    $this->pick = (int) $pick;
  }

  private function hydrate($donnees){
    foreach ($donnees as $key => $value){
      $method = 'set'.ucfirst($key);
      if(method_exists($this,$method)){
        $this->$method($value);
      }
    }
  }
}
?>
