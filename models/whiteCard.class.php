<?php
class whiteCard{

  /**
  *@var int $id id de la carte
  *@var string $textCard, texte de la carte"
  *@var string $texte, texte de la carte
  *@var pdo $db, database
  */

  private $id;
  private $textCard;
  private $db;

  /**
    *@param PDO $db, connexion à la base de données
    */
  public function __construct($db){
    $this->db = $db;
    $this->generateWhiteCard();
  }

  /**
   *@return liste des cartes blanches
   */
  public function getWhiteCards(){
    $req = $this->db->query('SELECT * FROM whiteCards');
    return $req->fetchAll(PDO::FETCH_ASSOC);
  }

  public function generateWhiteCard(){
    $req = $this->db->query('SELECT * FROM whiteCards ORDER BY RAND() LIMIT 1');
    $card = $req->fetch();
    $this->id = $card['id'];
    $this->textCard = $card['textCard'];
  }

  public function id(){
    return $this->id;
  }

  public function textCard(){
    return $this->textCard;
  }

}
?>
