<?php
class db{
  private $host = 'localhost';
  private $dbname = 'cardsAgainst';
  private $pseudo = 'user';
  private $mdp = 'mdp';
  private $db;

  public function __construct(){
    $this->db = $this->connectDb();
  }

  /**
   * Connexion à la base donnée
   *@return PDO objet de connexion à la base de donnée
   */
  public function connectDb(){
    try{
      return new PDO('mysql:host='.$this->host.';dbname='.$this->dbname.';charset=utf8',$this->pseudo,$this->mdp, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
    }catch(Exception $e){
      die('ERROR POTATOES NOT FOUND :'.$e->getMessage());
    }
  }
}
?>
